package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    List<String> continueChoices = Arrays.asList("y", "n");
    
    public void run() {
        while(true) {
            System.out.println("Let's play round " + roundCounter);

            //Human and Computer choice
            String humanChoice = userChoice("Your choice (Rock/Paper/Scissors)?", rpsChoices);
            String computerChoice = randomChoice();
            String choiceString = ("Human chose " + humanChoice + ", computer chose " + computerChoice + ".");

            //Check who won
            if(isWinner(humanChoice, computerChoice)) {
                System.out.println(choiceString + " Human Wins!");
                humanScore++;
            }
            else if(isWinner(computerChoice, humanChoice)) {
                System.out.println(choiceString + " Computer Wins!");
                computerScore++;
            }
            else {
                System.out.println(choiceString + " It's a tie!");
            }
            System.out.println("Score: human " + humanScore + ", computer " + computerScore);

            //Ask if human wants to play again
            String continueAnswer = userChoice("Do you wish to continue playing? (y/n)?", continueChoices);
            if(continueAnswer.equals("n")){
                break;
            }
            roundCounter++;
        }
        System.out.println("Bye bye :)"); 
    }
     
    

    //randomChoice method
    public String randomChoice() {
        Random r = new Random();
        return rpsChoices.get(r.nextInt(rpsChoices.size()));
    }

    //isWinner method
    public boolean isWinner(String choice1, String choice2) {
        if(choice1.equals("paper")) {
            return choice2.equals("rock");
        }
        if(choice1.equals("scissors")) {
            return choice2.equals("paper");
        }
        else {
            return choice2.equals("scissors");
        }
    }

    //userChoice method
    public String userChoice(String prompt, List<String> validInput) {
        while(true) {
            String humanChoice = readInput(prompt);
            
            if(validateInput(humanChoice, validInput)) {
                return humanChoice;
            }
            else {
                System.out.println("I don't understand " + humanChoice + ". Try again.");
            }
        }
    }

    //validateInput method
    public Boolean validateInput(String input, List<String> validInput) {
        input = input.toLowerCase();
        return validInput.contains(input);
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt){
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
